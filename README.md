# powerMEIS3 Web Server

The pm3ws folder should be in the same folder of the pm3:

```
/home/
   | pm3
   | pm3ws
```

To start the server simply run:
> python3 pm3ws/server.py

To access the page open a browser and look for:

> 0.0.0.0:8080