# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

projectdir = 'open-pM3ws'

from multiprocessing import Process, Queue
import threading
import datetime
import time
import sys
import os

# main server library
from flask import Flask, render_template, request, Response
app = Flask(__name__)

# gzip compression library
from flask_compress import Compress

Compress(app)
# sets output options
import logging
home_path = os.path.dirname(os.path.realpath(__file__)).split('/'+projectdir)[0] + '/'
logging.basicConfig(filename=home_path+projectdir+'/server.log',level=logging.INFO)
#log = logging.getLogger('werkzeug')
#log.setLevel(logging.ERROR)

#add local libraries
from worker import Worker

# default host IP and zipfile directory, overwritten when used on TARS
host = 'localhost'
zipdir = 'jobs/'
port = 80

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  INDEX  - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

@app.route('/')
@app.route('/index', methods=['GET'])
def index():
	return render_template('pm3.html')

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  powerMEIS3 - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

@app.route('/pm3', methods=['GET'])
def pm3():
	return render_template('pm3.html')

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class keeper:
	def __init__(self):
		# maximum number of jobs
		self.Njobs = 10
		# maximum time a job may run, in seconds
		self.Tmax = 600
		# number of threads per job
		self.nThreads = 4

	# watcher function
	def watcher(self, jobs, debug):
		while True:
			try:
				if debug == True:
					print(str(datetime.datetime.now()).split('.')[0], end='\t')
					for i in range(self.Njobs):
						print(jobs[i].status, end='\t')
					print()
				for i in range(self.Njobs):
					# look for failed jobs
					if jobs[i].err != 0:
						self.stop_job(i)

					# look for jobs long created
					if jobs[i].status > 0 and (time.time() - jobs[i].start_time) > self.Tmax:
						self.stop_job(i)

					# look for jobs that suicided
					if jobs[i].status == -100:
						self.stop_job(i)

					# look for finished jobs and free them
					if jobs[i].status == -1 and (time.time() - jobs[i].death_time) > 3:
						jobs[i].__init__()

				time.sleep(1)
			except:
				pass
		watcher(jobs, debug)

	def allocate_job(self, job_uid):
		# retry for 10s to see if a slot becomes available before failure
		initial_time = time.time()
		while time.time()-initial_time < 10:
			# search for a free element in jobs array
			for i in range(self.Njobs):
				if jobs[i].status == 0:
					# first, mark the job as running
					jobs[i].status = 1
					# update the uid and nThreads variables
					jobs[i].uid = job_uid
					jobs[i].nThreads = self.nThreads
					jobs[i].zipfile = zipdir+jobs[i].uid+'.zip'
					# get the creation time
					jobs[i].start_time = time.time()
					return i
		return -1

	def find_job(self, job_uid):
		# retry for 10s to see if the process was initiated before failure
		initial_time = time.time()
		while time.time()-initial_time < 10:
			for i in range(self.Njobs):
				if jobs[i].uid == job_uid:
					return i
		return -1

	def stop_job(self, i):
		# set the job as finished
		if jobs[i].death_time == 0:
			jobs[i].death_time = time.time()
		jobs[i].status = -1
		jobs[i].stop_worker()

# define a job class and open a global jobs array to store it
class job:
	#opening variables
	def __init__(self):
		self.err = 0					# job errors
		self.status = 0					# job status
		self.uid = 0					# job id
		self.start_time = 0				# time of creation
		self.death_time = 0				# time until destruction
		self.n = 0						# number of outputs already sent to client

		self.nThreads = 1				# number of simulation threads

		# communication with the child process
		self.queue_in = Queue()			# queue to send status updates
		self.queue_out = Queue()		# queue to receive data updates

		# file to be simulated
		self.zipfile = None				# zipfile FILE variable

		# instantiate a worker, it will spawn a process to compute the job
		self.worker = Worker()

	# spawn a new worker process
	def start_worker(self):
		p = Process(target=self.worker.start, args=(self,))
		p.start()
		p.join()

	# stop the worker process
	def stop_worker(self):
		self.queue_in.put(-1)

	# get data from the worker
	def get_worker_data(self):
		# get the last data from the queue
		[status, err, progcount, data] = self.queue_out.get()

		# updates the simulation status, if it didn't already stopped
		if self.status != -1:
			self.status = status
			self.err = err

		return [progcount, data]

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

@app.route('/start_pm3', methods=['POST'])
def start_pm3():
	# create the job
	job_uid = request.form['uid']
	i = keeper.allocate_job(job_uid)
	if i < 0:
		return render_template('pm3.html')

	# start log entry
	if debug == True:
		with open(home_path+projectdir+'/server.log', 'a') as f:
			print('['+time.strftime("%d/%m/%Y %H:%M:%S")+'] '+jobs[i].uid+' --- /start', file=f)

	#jobs[i].zipfile = request.files['payload']

	# save a copy of the request
	request.files['payload'].save(jobs[i].zipfile)

	jobs[i].start_worker()

	#save error stats in the log file
	if debug == True:
		if jobs[i].err != 0:
			with open(home_path+projectdir+'/server.log', 'a') as f:
				print('['+time.strftime("%d/%m/%Y %H:%M:%S")+'] '+jobs[i].uid+' --- ERROR: '+str(jobs[i].err), file=f)

	return render_template('pm3.html')

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

@app.route('/stop_pm3', methods=['POST'])
def stop_pm3():
	job_uid = request.get_json()

	# search for the job from the uid given
	i = keeper.find_job(job_uid)
	if i < 0:
		return render_template('pm3.html')

	# stop the job
	keeper.stop_job(i)

	#save status in the log file
	if debug == True:
		with open(home_path+projectdir+'/server.log', 'a') as f:
			print('['+time.strftime("%d/%m/%Y %H:%M:%S")+'] '+jobs[i].uid+' --- /stop', file=f)

	return render_template('pm3.html')

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

@app.route('/progress_pm3/<job_uid>')
def progress_pm3(job_uid):
	# message generator
	def generate(m):
		yield m

	# search for the job from the uid given
	i = keeper.find_job(job_uid)
	if i == -1:
		payload = 'data: { "err": 400 }\n\n'
		return Response(generate(payload), mimetype='text/event-stream')

	# check if the process is running
	if jobs[i].status == 0:
		payload = 'retry: 100\ndata: { "status": 0, "err":'+str(jobs[i].err)+' }\n\n'

	# check if the process is simulating
	elif jobs[i].status > 0:
		# collect the data to send back to client
		[progcounts, data] = jobs[i].get_worker_data()

		payload = 'retry: 100\ndata: { "status":'+str(jobs[i].status)+', "err": '+str(jobs[i].err)+', "progcount": '+str(progcounts)+', '+data+'}\n\n'

	# check if the process was killed, then send the last batch of data
	elif jobs[i].status < 0:
		# collect the data to send back to client
		[progcounts, data] = jobs[i].get_worker_data()

		payload = 'data: { "status": 100, "err": '+str(jobs[i].err)+', "progcount": '+str(progcounts)+', '+data+'}\n\n'

	return Response(generate(payload), mimetype='text/event-stream')

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  MAIN - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

# instanciate the keeper function
keeper = keeper()
# open the jobs list
jobs = [job() for i in range(keeper.Njobs)]

if __name__ == "__main__":
	# debug option, to enable run as follows: '$ python server.py yes'
	debug = False
	if len(sys.argv) > 1:
		debug = bool(sys.argv[1]);

	# when running TARS server, overwrite these options
	if len(sys.argv) > 2:
		if bool(sys.argv[2]):
			host = '0.0.0.0'
			port = 8080
			zipdir = home_path+projectdir+'/jobs/'

	# start the keeper watcher deamon
	keeper.back = threading.Thread(target=keeper.watcher, args=(jobs, debug))
	keeper.back.start()

	app.run(host=host,port=port,threaded=True, debug=False)
