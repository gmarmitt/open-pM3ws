var errors_descriptions = [
		{},
		[
			{
				'id': 100,
				'msg': 'Error! Layer randomization failed.',
				'tip': 'Possible error in the layers description.'
			},
			{
				'id': 101,
				'msg': 'Error! Matrix randomization failed.',
				'tip': 'Possible error in the matrices description.'
			},
			{
				'id': 102,
				'msg': 'Error! Matrix randomization failed.',
				'tip': 'Possible error in the nx matrix size.'
			},
			{
				'id': 103,
				'msg': 'Error! Matrix randomization failed.',
				'tip': 'Possible error in the ny matrix size.'
			},
			{
				'id': 104,
				'msg': 'Error! Matrix randomization failed.',
				'tip': 'Possible error in the nz matrix size.'
			},
			{
				'id': 105,
				'msg': 'Error! Element randomization failed.',
				'tip': 'Possible error in the elements description.'
			},
		], 
		[
			{
				'id': 200,
				'msg': 'Error! Loading one matrix file failed.',
				'tip': 'Verify if the matrices files were correctly uploaded.'
			},
			{
				'id': 201,
				'msg': 'Error! Loading one matrix file failed.',
				'tip': 'Verify if the matrices files were correctly uploaded.'
			},
			{
				'id': 202,
				'msg': 'Error! Decompressing one matrix file failed.',
				'tip': 'Verify the matrix format for possible inconsistencies.'
			},
		], 
		[
			{
				'id': 300,
				'msg': 'Error! Unzipping the received package failed.',
				'tip': 'Possible curruption during upload, please try again.'
			},
			{
				'id': 301,
				'msg': 'Error! Loading the cpm file failed.',
				'tip': 'Possible error in the elements and compounds description.'
			},
			{
				'id': 302,
				'msg': 'Error! Pre-simulation procedures failed.',
				'tip': 'Possible error in the detector description.'
			},
			{
				'id': 303,
				'msg': 'Error! Memory allocation for tables failed.',
				'tip': 'Possible error in the output description.'
			},
			{
				'id': 304,
				'msg': 'Error! Kinematic factor calculation failed.',
				'tip': 'Possible error in the elements description.'
			},
			{
				'id': 305,
				'msg': 'Error! Cross section calculation failed.',
				'tip': 'Possible error in the elements description.'
			},
			{
				'id': 306,
				'msg': 'Error! Stopping force calculation failed.',
				'tip': 'Possible error in the compounds description.'
			},
			{
				'id': 307,
				'msg': 'Error! Energy straggling calculation failed.',
				'tip': 'Possible error in the compounds description.'
			},
			{
				'id': 308,
				'msg': 'Error! Charge fraction calculation failed.',
				'tip': 'Possible error in the compounds description.'
			},
		],
		[
			{
				'id': 400,
				'msg': 'Error! Reconnection failed.',
				'tip': 'The simulation thread has finished, please restart.'
			},
		]
	];
