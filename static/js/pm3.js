// create the module and name it app
var App = angular.module('App', ['ui.bootstrap', 'LocalStorageModule']);

// cache prefix
App.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('pM3');
});

var supportsPassive = false;
try {
  var opts = Object.defineProperty({}, 'passive', {
    get: function() {
      supportsPassive = true;
    }
  });
  window.addEventListener("test", null, opts);
} catch (e) {}

// accordion status
App.factory('status', function(){
	var status = {
			systemPane : {
				selected : 0,
			},
			filesPane : {
				selected : 0,
			},
			samplePane : {
				selected : 0,
			},
			algoForm : {},
			calcForm : {},
			outpForm : {},
			beamForm : {},
			deteForm : {},
			elemForm : {},
			matrixForm : {},
			compForm : {},
			compelemForm : {},
			layerForm : {},
			layermatrixForm : {},
			graph_simuForm : {},
		};
	return{
		status : status,
	};
});

// prmt functions
App.factory('vars', function(localStorageService){
	// initiate the input variables
	var prmt = {};
	var files = {};
	var graphdata = Array();

	// initialize all the varibales
	var initialize_vars = function(){
		// delete all the attributes of prmt
		for (var attr in prmt) delete prmt[attr];

		// copy all the atributes from prmt_initial_values to the actual prmt
		obj = JSON.parse(JSON.stringify(prmt_initial_values));
		for (var attr in obj) {
			if (obj.hasOwnProperty(attr)) prmt[attr] = obj[attr];
		};

		// MUST use this line (HAVE NO IDEA WHY?!?!) instead of 'files = {};'
		for (var attr in files) delete files[attr];
	};

	var post_read_cpm = function() {
		var iEl=0; var iCp=0; var iMt=0; var iLa=0; var iEx=0; var iSi=0;

		varname = 'element_s_1';
		while(typeof prmt[varname] != 'undefined'){iEl++;varname = 'element_s_'+String(iEl+1);};

		varname = 'compound_s_1';
		while(typeof prmt[varname] != 'undefined'){iCp++;varname = 'compound_s_'+String(iCp+1);};

		varname = 'matrix_s_1';
		while(typeof prmt[varname] != 'undefined'){iMt++;varname = 'matrix_s_'+String(iMt+1);};

		varname = 'layer_s_1';
		while(typeof prmt[varname] != 'undefined'){iLa++;varname = 'layer_s_'+String(iLa+1);};

		varname = 'expdata_s_1';
		while(typeof prmt[varname] != 'undefined'){iEx++;varname = 'expdata_s_'+String(iEx+1);};

		varname = 'simdata_s_1';
		while(typeof prmt[varname] != 'undefined'){iSi++;varname = 'simdata_s_'+String(iSi+1);};

		prmt.header.nEl = iEl;
		prmt.header.nCp = iCp;
		prmt.header.nMt = iMt;
		prmt.header.nLa = iLa;
		prmt.header.nEx = iEx;
		prmt.header.nSi = iSi;

		// guarantees that all element fractions are initiated
		for(iCp=1; iCp<=prmt.header.nCp; iCp++)
			for(iEl=1; iEl<=prmt.header.nEl; iEl++)
				if(typeof prmt['compound_s_'+iCp]['_p_element_s_'+iEl] == 'undefined')
					prmt['compound_s_'+iCp]['_p_element_s_'+iEl] = 0;

		// guarantees that all matrix fractions are initiated
		for(iLa=1; iLa<=prmt.header.nLa; iLa++)
			for(iMt=1; iMt<=prmt.header.nMt; iMt++)
				if(typeof prmt['layer_s_'+iLa]['_p_matrix_s_'+iMt] == 'undefined')
					prmt['layer_s_'+iLa]['_p_matrix_s_'+iMt] = 0;

		// compatibility with older version, may be removed after some months of support
		if(iSi >= 1){
			if(typeof prmt['simdata_s_1'].norm 			!='undefined'){
				prmt['output'].norm = prmt['simdata_s_1'].norm;
				delete prmt['simdata_s_1'].norm;
			};
			if(typeof prmt['simdata_s_1'].chi2red 		!='undefined'){
				prmt['output'].chi2red = prmt['simdata_s_1'].chi2red;
				delete prmt['simdata_s_1'].chi2red;
			};
			if(typeof prmt['simdata_s_1'].chi2red_s_xmin!='undefined'){
				prmt['output'].chi2red_s_xmin = prmt['simdata_s_1'].chi2red_s_xmin;
				delete prmt['simdata_s_1'].chi2red_s_xmin;
			};
			if(typeof prmt['simdata_s_1'].chi2red_s_xmax!='undefined'){
				prmt['output'].chi2red_s_xmax = prmt['simdata_s_1'].chi2red_s_xmax;
				delete prmt['simdata_s_1'].chi2red_s_xmax;
			};
		};
	};

	// generate a cpm from the json object
	var generate_zip = function(save_zip_filename){
		// instantiate a zip object
		var zip = new JSZip();

		// put all matrices inside
		for(iMt=1; iMt<=prmt.header.nMt; iMt++) if(files['matrix_s_'+iMt].text != '')
			zip.file(prmt['matrix_s_'+iMt].file, files['matrix_s_'+iMt].text);

		// put experimental file inside
		nEx = parseInt(prmt.header.nEx);
		if(nEx > 0){
			var file_text = '';
			for(i=0; i<graphdata[0].xraw.length; i++)
				file_text += graphdata[0].xraw[i] + '\t' + graphdata[0].yraw[i] + '\n';
			zip.file(prmt['expdata_s_1'].file, file_text);
		};

		// put all simulations files inside
		for(iSi=1; iSi<=prmt.header.nSi; iSi++){
			// 1D graph
			var file_text = '';
			for(i=0; i<graphdata[iSi].length; i++)
				file_text += graphdata[iSi].energy[i] + '\t' + graphdata[iSi].counts1D[i] + '\n';
			zip.file(prmt['simdata_s_'+iSi].file+'.dat', file_text);
			// 2D map
			if(graphdata[iSi].counts2D.length > 1){
				var file_text = '#E(keV)\t';
				for(j=0; j<graphdata[iSi].angle.length; j++)
					file_text += graphdata[iSi].angle[j]+'\t';
				file_text += '\n';

				for(i=0; i<graphdata[iSi].energy.length; i++){
					file_text += graphdata[iSi].energy[i] + '\t';
					for(j=0; j<graphdata[iSi].angle.length; j++)
						file_text += graphdata[iSi].counts2D[i][j]+'\t';
					file_text += '\n';
				}
				zip.file(prmt['simdata_s_'+iSi].file+'.map', file_text);
			}
		}

		// create the cpm text to be saved/sent
		var cpm = '';
		for(section in prmt){
			cpm += '['+section.replace(/_s_/g, ' ')+']\n';
			for(key in prmt[section]){
				if(typeof prmt[section][key] != 'undefined'){
					cpm += key.replace(/_s_/g, ' ').replace(/_p_/g, '%') + ' = ' + prmt[section][key] + '\n';
				};
			};
			cpm += '\n';
		};

		// add the cpm file and generate the zipped blob
		zip.file('pm3.cpm', cpm);
		var zipData = zip.generate({ type: 'blob', compression: 'DEFLATE' });

		return zipData;
	};

	// generate a json object from the cpm text
	var read_cpm = function(text){
		// split in lines
		var lines_raw = text.split('\n');

		// first we remove the white spaces
		var lines = Array();
		for(i=0; i<lines_raw.length; i++){
			if(lines_raw[i].length != 1 && lines_raw[i] != '' && lines_raw[i][0] != '#'){
				lines.push(lines_raw[i].trim());
			};
		};

		for(i=0; i<lines.length; i++){
			ind_final = lines[i].length-1;
			// if the line is a section header
			if(lines[i][0] ==  '[' && lines[i][ind_final] ==  ']'){
				section = lines[i].substring(1, ind_final).replace(/ /g, '_s_');
				// now we create the section
				if(typeof prmt[section] == 'undefined'){
					prmt[section] = {};
				};
			// else it is a key=value statement
			} else{
				line = lines[i].split('=');
				key = line[0].trim().replace(/ /g, '_s_').replace(/%/g, '_p_');
				value = line[1].trim();
				if(value != 'undefined')
					prmt[section][key] = value;
			};
		};
	};

	// read the files inside a zip
	var read_zipped_files = function(zip){
		nSi = parseInt(prmt.header.nSi);
		for(iSi=1; iSi<=nSi; iSi++){
			// 1D graph
			var file = zip.file(prmt['simdata_s_'+iSi].file+'.dat');
			var text = file.asText();
			var lines = text.split('\n');
			for(iLine=0; iLine<lines.length-1; iLine++){
				var data = lines[iLine].trim().split(/[\t\s]+/);
				graphdata[iSi].energy.push(parseFloat(data[0]));
				graphdata[iSi].counts1D.push(parseFloat(data[1]));
			};
			graphdata[iSi].name = prmt['simdata_s_'+iSi].file;
			graphdata[iSi].length = graphdata[iSi].energy.length;
			// 2D map
			var file = zip.file(prmt['simdata_s_'+iSi].file+'.map')
			if(file != null){
				var text = file.asText();
				var lines = text.split('\n');
				graphdata[iSi].angle = lines[0].trim().split(/[\t\s]+/).slice(1);
				for(iLine=1; iLine<lines.length-1; iLine++){
					var data = lines[iLine].trim().split(/[\t\s]+/);
					graphdata[iSi].counts2D.push(Array());
					for(j=1; j<data.length; j++)
						graphdata[iSi].counts2D[iLine-1].push(parseFloat(data[j]));
				}
			}
		};

		nEx = parseInt(prmt.header.nEx);
		if(nEx > 0){
			var text = zip.file(prmt['expdata_s_1'].file).asText();
			var lines = text.split('\n');
			for(iLine=0; iLine<lines.length; iLine++){
				var data = lines[iLine].trim().split(/[\t\s]+/);
				graphdata[0].xraw.push(parseFloat(data[0]));
				graphdata[0].yraw.push(parseFloat(data[1]));
			};
			graphdata[0].x = Array(lines.length);
			graphdata[0].y = Array(lines.length);
			graphdata[0].name = prmt['expdata_s_1'].file;
			graphdata[0].length = graphdata[0].x.length;
			if(typeof prmt['expdata_s_1'].a == 'undefined') prmt['expdata_s_1'].a = 1;
			if(typeof prmt['expdata_s_1'].b == 'undefined') prmt['expdata_s_1'].b = 0;
		};
	};

	// read the matrices inside a zip file
	var read_zipped_mtx = function(zip){
		nMt = parseInt(prmt.header.nMt);
		for(iMt=1; iMt<=nMt; iMt++){
			files['matrix_s_'+iMt] = {};
			var file_splited = prmt['matrix_s_'+iMt].file.split('.');

      if(prmt['matrix_s_'+iMt].file in zip.files){
        files['matrix_s_'+iMt].text = zip.file(prmt['matrix_s_'+iMt].file).asText();
      }
      else {
        files['matrix_s_'+iMt].text = '';
      }

			if(file_splited[file_splited.length-1] == 'mtx'){
				prmt['matrix_s_'+iMt].type = 'pM3';

				var lines = files['matrix_s_'+iMt].text.split('\n');
				var numbers = lines[0].trim().split(/\s/);
				if(	typeof prmt['matrix_s_'+iMt].nx == 'undefined' ||
					typeof prmt['matrix_s_'+iMt].ny == 'undefined' ||
					typeof prmt['matrix_s_'+iMt].nz == 'undefined')
					{
					if(numbers.length >= 3) {
						prmt['matrix_s_'+iMt].nx = numbers[0];
						prmt['matrix_s_'+iMt].ny = numbers[1];
						prmt['matrix_s_'+iMt].nz = numbers[2];
					}
					else{
						prmt['matrix_s_'+iMt].nx = 0;
						prmt['matrix_s_'+iMt].ny = 0;
						prmt['matrix_s_'+iMt].nz = 0;
					};
				};
				if(typeof prmt['matrix_s_'+iMt].phi == 'undefined'){
					if(numbers.length >= 4)
						prmt['matrix_s_'+iMt].phi = numbers[3];
					else
						prmt['matrix_s_'+iMt].phi = 0;
				};
			};
			if(file_splited[file_splited.length-1] == 'prf' || file_splited[file_splited.length-1] == 'prf2')
				prmt['matrix_s_'+iMt].type = 'TRI3DYN';
		};
	};

	// save the current version of prmt, files in cache
	var save_cache = function(){
		if(localStorageService.isSupported){
			localStorageService.set('prmt', JSON.stringify(prmt));
			localStorageService.set('files', JSON.stringify(files));
		};
	};

	// update the current version of graphdata in cache
	var update_cache = function(){
		if(localStorageService.isSupported){
			localStorageService.set('graphdata', JSON.stringify(graphdata));
		};
	};

	// first initialization
	initialize_vars();
	post_read_cpm();
	//localStorageService.clearAll();

	// read the cached version of prmt, files or graphdata
	if(localStorageService.isSupported){
		if(localStorageService.get('prmt') != null) prmt = JSON.parse(localStorageService.get('prmt'));
		if(localStorageService.get('files') != null) files = JSON.parse(localStorageService.get('files'));
		if(localStorageService.get('graphdata') != null) graphdata = JSON.parse(localStorageService.get('graphdata'));
	};

	return{
		prmt : prmt,
		files : files,
		graphdata : graphdata,
		initialize_vars : initialize_vars,
		read_cpm : read_cpm,
		generate_zip : generate_zip,
		post_read_cpm : post_read_cpm,
		read_zipped_mtx : read_zipped_mtx,
		read_zipped_files : read_zipped_files,
		save_cache : save_cache,
		update_cache : update_cache,
	};
});

// accordion status
App.factory('graph', function($rootScope, vars){
	var updated = false;
	var graph_type = '1D';
	var d3 = Plotly.d3;
	var gd3 = d3.select('div[id=livegraph]');
	var gd = gd3.node();

	var clean_expdata = {
		x: [],
		y: [],
		xraw: [],
		yraw: [],
		length: 0,
		type:'scatter',
		mode: 'markers',
		size: 10,
		symbol: 'circle',
		//hoverinfo: 'none',
	};
	var clean_simdata = {
		x: [],
		y: [],
		z: [],
		energy: [],
		angle: [],
		counts1D: [],
		counts2D: [],
		length: 0,
		type:'scatter',
		mode: 'line',
		//hoverinfo: 'x',
		colorscale: 'Earth',
	};

	var layout1D  = {
		margin:{
			l: 50,
			r: 10,
			b: 40,
			t: 10,
		},
		xaxis:{
			title:'Energy (keV)',
		},
		yaxis:{
			exponentformat: 'e',
		},
		legend:{
			orientation: 'h',
		},
	};
	var layout1D_energyloss  = {
		margin:{
			l: 50,
			r: 10,
			b: 40,
			t: 10,
		},
		xaxis:{
			title:'Energy Loss (eV)',
		},
		yaxis:{
			exponentformat: 'e',
		},
		legend:{
			orientation: 'h',
		},
	};
	var layout2D  = {
		margin:{
			l: 50,
			r: 10,
			b: 40,
			t: 10,
		},
		xaxis:{
			title:'Angle (°)',
		},
		yaxis:{
			title:'Energy (keV)',
		},
		legend:{
			orientation: 'h',
		},
	};

	window.onresize = function() {
		Plotly.Plots.resize(gd);
	};

	// graph functions
	var create_graph = function(){
		vars.graphdata.push(clean_expdata);
		Plotly.newPlot(gd, vars.graphdata, layout1D);
	};

	// reset exp graphdata
	var reset_expdata = function(){
		var expdata = JSON.parse(JSON.stringify(clean_expdata));
		vars.graphdata[0] = expdata;
		vars.prmt.header.nEx = 0;
	};

	// reset sim graphdata
	var reset_simdata = function(nSi_new){
		var nSi_old = parseInt(vars.prmt.header.nSi);

		// delete sim graphdata
		while(vars.graphdata.length > 1) vars.graphdata.pop();

		// resize prmt's simdata
		for(iSi=nSi_old; iSi>nSi_new; iSi--) delete vars.prmt['simdata_s_'+iSi];
		for(iSi=nSi_old+1; iSi<=nSi_new; iSi++) vars.prmt['simdata_s_'+iSi] = {};

		// reconstruct clean sim graphdata
		for(iSi=1; iSi<=nSi_new; iSi++){
			var simdata = JSON.parse(JSON.stringify(clean_simdata));
			vars.graphdata.push(simdata);
		};
		vars.prmt.header.nSi = nSi_new;
	};

	// load sim graphdata
	var load_simdata = function(payload_data){
		var nSi = parseInt(vars.prmt.header.nSi);

		// reconstruct clean sim graphdata
		for(iSi=1; iSi<=nSi; iSi++){
			vars.graphdata[iSi].name = payload_data[iSi].name;
			vars.graphdata[iSi].length = payload_data[0].energy.length;
			vars.prmt['simdata_s_'+iSi].file = payload_data[iSi].name;
		};
		vars.prmt.header.nSi = nSi;
	};

	// update exp data
	var load_expdata = function(x, y, name){
		// build graphdata
		vars.graphdata[0].xraw = x;
		vars.graphdata[0].yraw = y;
		vars.graphdata[0].x = Array(x.length);
		vars.graphdata[0].y = Array(y.length);
		vars.prmt['expdata_s_1'] = {};
		vars.prmt['expdata_s_1'].file = name;
		vars.prmt['expdata_s_1'].a = 1;
		vars.prmt['expdata_s_1'].b = 0;
		vars.graphdata[0].name = name;
		vars.graphdata[0].length = vars.graphdata[0].x.length;

		vars.prmt.header.nEx = 1;
	};

	var update_expdata = function(){
		if(	vars.prmt.header.nEx > 0){
			var a = parseFloat(vars.prmt['expdata_s_1'].a);
			var b = parseFloat(vars.prmt['expdata_s_1'].b);
			var size = vars.graphdata[0].x.length;
			for(i=0; i<size; i++)
				vars.graphdata[0].x[i] = vars.graphdata[0].xraw[i] * a + b;
			vars.graphdata[0].y = vars.graphdata[0].yraw;
		};
		Plotly.redraw(gd);
	};

	// update sim data
	var update_simdata = function(payload_data){
		if(payload_data.length == 0) return;

		var nSi = parseInt(vars.prmt.header.nSi);

		// build graphdata
		for(iSi=1; iSi<=nSi; iSi++){
			vars.graphdata[iSi].energy = payload_data[0].energy;
			vars.graphdata[iSi].angle = payload_data[0].angle;
			vars.graphdata[iSi].counts1D = payload_data[iSi].h1D;
			vars.graphdata[iSi].counts2D = payload_data[iSi].h2D;
		};
	};

	var change_layout = function(){
		var nSi = parseInt(vars.prmt.header.nSi);

		if(vars.prmt.output.type == '1D'){
			for(iSi=1; iSi<=nSi; iSi++)	vars.graphdata[iSi].type = 'scatter';
			Plotly.newPlot(gd, vars.graphdata, layout1D);
		};
		if(vars.prmt.output.type == '1D_energyloss'){
			for(iSi=1; iSi<=nSi; iSi++)	vars.graphdata[iSi].type = 'scatter';
			Plotly.newPlot(gd, vars.graphdata, layout1D_energyloss);
		};
		if(vars.prmt.output.type == '2D'){
			for(iSi=1; iSi<=nSi; iSi++)	vars.graphdata[iSi].type = 'heatmap';
			Plotly.newPlot(gd, vars.graphdata.slice(1), layout2D);
		};
	};

	var update_graph = function(){
		// reapply normalization to the counts1D simdata values
		var nSi = parseInt(vars.prmt.header.nSi);
		var norm = parseFloat(vars.prmt['output'].norm);

		if(vars.prmt.output.type == '1D'){
			for(iSi=1; iSi<=nSi; iSi++){
				vars.graphdata[iSi].x = vars.graphdata[iSi].energy;
				vars.graphdata[iSi].y = Array(vars.graphdata[iSi].length);
				for(i=0; i<vars.graphdata[iSi].length; i++)
					vars.graphdata[iSi].y[i] = norm * vars.graphdata[iSi].counts1D[i];
			}
		};
		if(vars.prmt.output.type == '1D_energyloss'){
			for(iSi=1; iSi<=nSi; iSi++){
				vars.graphdata[iSi].x = Array(vars.graphdata[iSi].length);
				vars.graphdata[iSi].y = Array(vars.graphdata[iSi].length);
				for(i=0; i<vars.graphdata[iSi].length; i++){
					vars.graphdata[iSi].x[i] = (vars.prmt.beam.E0-vars.graphdata[iSi].energy[i])*1e3;
					vars.graphdata[iSi].y[i] = norm * vars.graphdata[iSi].counts1D[i];
				};
			}
		};
		if(vars.prmt.output.type == '2D'){
			for(iSi=1; iSi<=nSi; iSi++){
				vars.graphdata[iSi].x = vars.graphdata[iSi].angle;
				vars.graphdata[iSi].y = vars.graphdata[iSi].energy;
				vars.graphdata[iSi].z = vars.graphdata[iSi].counts2D;
				}
		};
		// redraw the graph
		Plotly.redraw(gd);

		// update the graphdata in cache
		vars.update_cache();
	};

	// update the chi2 value
	var calculate_chi2 = function(){
		var nSi = parseInt(vars.prmt.header.nSi);
		var nEx = parseInt(vars.prmt.header.nEx);

		if(nEx <= 0 && nSi <= 0) return;

		// zeroes the chi2 values
		var chi2red = 0; var N = 0;

		for(iSi=1; iSi<=nSi; iSi++){
			var i;
			for(i=0; i<vars.graphdata[0].length; i++){
				exp_x = vars.graphdata[0].x[i];
				exp_y = vars.graphdata[0].y[i];
				if(	exp_x >= parseFloat(vars.prmt['output'].chi2red_s_xmin) &&
					exp_x < parseFloat(vars.prmt['output'].chi2red_s_xmax)){

					sim_y = interpolate(vars.graphdata[iSi], exp_x);

					if(sim_y != null){
						chi2red += Math.pow((exp_y - sim_y + Math.min(1, exp_y)), 2) / Math.max(exp_y, 1);
						N += 1;
					};
				};
			};
		};
		chi2red *= 1 / (N - 1);
		vars.prmt['output'].chi2red = chi2red.toPrecision(5);
	};

	var calculate_norm = function(){
		var nSi = parseInt(vars.prmt.header.nSi);
		var nEx = parseInt(vars.prmt.header.nEx);

		var sim2_sum = 0;
		var expsim_sum = 0;

		if(nEx <= 0 || nSi <= 0) return;

		// create a new simdata object, based on simdata 1
		var simdata = JSON.parse(JSON.stringify(clean_simdata));
		simdata.length = vars.graphdata[1].length;
		simdata.x = vars.graphdata[1].energy;
		simdata.y = Array(simdata.length);

		// clear the y array to then sum all counts1D components into it
		for(i=0; i < simdata.length; i++)
			simdata.y[i] = 0;

		// sum all simulation components
		for(iSi=1; iSi<nSi+1; iSi++)
			for(i=0; i < simdata.length; i++)
				simdata.y[i] += parseFloat(vars.graphdata[iSi].counts1D[i]);

		// now, goes through all experimental points
		var i;
		for(i=0; i<vars.graphdata[0].length; i++){
			exp_x = vars.graphdata[0].x[i];
			exp_y = vars.graphdata[0].y[i];
			if(	exp_x >= parseFloat(vars.prmt['output'].chi2red_s_xmin) &&
				exp_x < parseFloat(vars.prmt['output'].chi2red_s_xmax)){

				sim_y = interpolate(simdata, exp_x);

				if(sim_y != null){
					sim2_sum 	+= sim_y*sim_y / Math.max(exp_y, 1);
					expsim_sum 	+= exp_y*sim_y / Math.max(exp_y, 1);
				};
			};
		};
		var norm = expsim_sum / sim2_sum;
		vars.prmt['output'].norm = norm.toPrecision(5);

	};

	create_graph();

	return{
		create_graph : create_graph,
		reset_expdata : reset_expdata,
		reset_simdata : reset_simdata,
		load_expdata : load_expdata,
		load_simdata : load_simdata,
		update_simdata : update_simdata,
		update_expdata : update_expdata,
		change_layout : change_layout,
		update_graph : update_graph,
		calculate_norm : calculate_norm,
		calculate_chi2 : calculate_chi2,
		updated : updated,
	};
});

// functions to aid in the client-server connection
App.factory('connect', function($rootScope, $filter, $http, vars, status, graph, localStorageService){
	var progbar = {};

	var reset_progbar = function(){
		// generate a unique id to communicate with the server
		progbar.uid = guid()

		// progbar variables
		progbar.first_data_package = true;
		progbar.message = 'Standby';
		progbar.updated = true;
		progbar.count = 0;
		progbar.width = 0;
		progbar.max = parseInt(parseFloat(vars.prmt.simulation.nI));
	};

	// fire the request for the simulation
	var start_simulation = function(){
		// generate the zip from prmt object
		zipData = vars.generate_zip();

		// save cache
		vars.save_cache();
		progbar_save_cache();

		// reset progress bar parameters
		reset_progbar();

		// set progbar initial state
		progbar.message = 'Sending';

		// opens the listening channel
		data_listener();

		var formData = new FormData();
		formData.append('uid', progbar.uid);
		formData.append('payload', zipData);

		var request = {
					method: 'POST',
					url: '/start_pm3',
					data: formData,
					headers: {
						'Content-Type': undefined
					}
				};
		$http(request);
	};

	var stop_simulation = function(){
		listener_source.close();
		$http.post('/stop_pm3', $filter('json')(progbar.uid));

		// save cache
		progbar.message = 'Stopped';
		progbar_save_cache();

		setTimeout(function(){
			// if the website remains stopped, put it to standby mode
			if(progbar.message == 'Stopped' || progbar.message == 'Interrupted') progbar.message = 'Standby';
			$rootScope.$apply();
		}, 1000);

		// force the last graph update
		graph.update_graph();
	};

	// create a listener to receive stream of data
	var data_listener = function(){
		// create listener
		listener_source = new EventSource('/progress_pm3/'+progbar.uid);

		// receive data comming from the server
		listener_source.onmessage = function (payload_text) {
			setTimeout(function () {data_received(payload_text);}, 10);
			//data_received(payload_text);
		};
	};

	var data_received = function(payload_text){
			// decode the JSON text
			var payload = JSON.parse(payload_text.data);

			if(payload.err == '0'){		// if there is no error
				if(payload.status == '0'){		// waiting for simulation to start
					progbar.message = 'Initializing';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '10'){		// waiting for simulation to start
					progbar.message = 'Initializing';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '20'){
					progbar.message = 'Reading cpm';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '30'){
					progbar.message = 'Building matrix';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '40'){
					progbar.message = 'Cross sections';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '50'){
					progbar.message = 'Stopping forces';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '60'){
					progbar.message = 'Energy Straggling';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '70'){
					progbar.message = 'Neutralization';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '80'){
					progbar.message = 'Dielectric func.';
					// force update variables
					$rootScope.$apply();
					return{};
				};
				if(payload.status == '100'){		// data is being received
					// progress bar total width
					var progbarHtmlElement = document.getElementById('progress-outside'),
					progbarStyle = window.getComputedStyle(progbarHtmlElement);
					progbar.total_width = progbarStyle.getPropertyValue('width').split('px')[0];

					// new progress bar values
					progbar.count = parseInt(payload.progcount);
					progbar.width = progbar.total_width*progbar.count/progbar.max;
					progbar.message = 'Running '+parseInt(100*progbar.count/progbar.max)+'%';

					// save current progress bar parameters to cache
					progbar_save_cache();

					// on the first package, update the graph lines with the incoming data
					if(progbar.first_data_package == true){
						progbar.first_data_package = false;
						graph.reset_simdata(payload.data.length-1);
						graph.load_simdata(payload.data);
						graph.change_layout();
					};

					// replot with the new data
					graph.update_simdata(payload.data);
					if (!document.hidden) graph.update_graph();

					// if simulation ended, stop listener
					if(progbar.count >= progbar.max){
						stop_simulation();
					};

					// force update variables
					$rootScope.$apply();
				};
			} else{
				stop_simulation();
				progbar.message = 'Interrupted';
				var err = parseInt(payload.err)
				var err_section = parseInt(err/100)
				var err_number = err - err_section*100;
				var errors = JSON.parse(JSON.stringify(errors_descriptions));
				alert('ID:'+errors[err_section][err_number].id+'\n'+
							errors[err_section][err_number].msg+'\n'+
							errors[err_section][err_number].tip);
				return{};
			};
		};

	// save the current version of progbar in cache
	var progbar_save_cache = function(){
		if(localStorageService.isSupported){
			localStorageService.set('progbar', JSON.stringify(progbar));
		};
	};

	// progbar initialization
	reset_progbar();

	// read the cached version of prmt, files or graphdata
	if(localStorageService.isSupported){
		if(localStorageService.get('progbar') != null){
			progbar = JSON.parse(localStorageService.get('progbar'));
			if(progbar.message.split(' ')[0] == 'Running'){
				progbar.message = 'Reconnecting';
				data_listener();
			}
			else{
				progbar.message = 'Standby';
			};
		};
	};

	return{
		progbar : progbar,
		reset_progbar : reset_progbar,
		start_simulation : start_simulation,
		stop_simulation : stop_simulation,
	};
});

// create the controller and inject Angular's factories
App.controller('appController', function($rootScope, $scope, $uibModal, $location, $anchorScroll, vars, connect, status, graph) {
	// expose some variables
	$scope.progbar = connect.progbar;
	$scope.prmt = vars.prmt;
	$scope.simulation_is_running = false;

	$scope.scrollTo = function(id) {
		$location.hash(id);
		$anchorScroll();
	};

	$scope.select_MEIS = function(){
		vars.prmt.header.mode = 'MEIS';
		vars.prmt.simulation.algorithm = 'ssct';
		vars.prmt.simulation.cross_s_section = 'correc';
		vars.prmt.simulation.line_s_shape = 'EMG';
		vars.prmt.calculations.neutralization = 'CasP';
		vars.prmt.detector.type = 'electro';
		vars.prmt.beam.type = 'ion';
		vars.prmt.calculations.dielec_s_function = 'off';
	};
	$scope.select_RBS = function(){
		vars.prmt.header.mode = 'RBS';
		vars.prmt.simulation.algorithm = 'ssct';
		vars.prmt.simulation.cross_s_section = 'Rutherford';
		vars.prmt.simulation.line_s_shape = 'gauss';
		vars.prmt.calculations.neutralization = 'off';
		vars.prmt.detector.type = 'electro';
		vars.prmt.beam.type = 'ion';
		vars.prmt.calculations.dielec_s_function = 'off';
	};
	$scope.select_NRAP = function(){
		vars.prmt.header.mode = 'NRA/P';
		vars.prmt.simulation.algorithm = 'nrp';
		vars.prmt.simulation.cross_s_section = 'Rutherford';
		vars.prmt.simulation.line_s_shape = 'gauss';
		vars.prmt.calculations.neutralization = 'off';
		vars.prmt.detector.type = 'electro';
		vars.prmt.beam.type = 'ion';
		vars.prmt.calculations.dielec_s_function = 'off';
	};
	$scope.select_ERBS = function(){
		vars.prmt.header.mode = 'ERBS';
		vars.prmt.simulation.algorithm = 'traj';
		vars.prmt.simulation.cross_s_section = 'Rutherford';
		vars.prmt.simulation.line_s_shape = 'gauss';
		vars.prmt.calculations.neutralization = 'off';
		vars.prmt.detector.type = 'electro';
		vars.prmt.beam.type = 'electron';
		vars.prmt.calculations.dielec_s_function = 'Mermin';
		vars.prmt.calculations.dielec_s_qmax = 10;
		vars.prmt.calculations.dielec_s_wmax = 1000;
		vars.prmt.calculations.dielec_s_dE = 10;
	};

	$scope.start_stop_simulation = function(){
		if($scope.progbar.message == 'Standby'){
			// disable start another simulation and enable to stop the current one
			connect.start_simulation();
			return{};
		}
		else{
			// disable stopping the simulation and enable to start another one
			connect.stop_simulation();
			return{};
		};
	};

	// open a new clean prmt
	$scope.new_cpm = function() {
		vars.initialize_vars();
		connect.reset_progbar();
		graph.reset_expdata();
		graph.reset_simdata(0);
	};

	$scope.open_cpm = function() {
		var input = document.getElementById('open_cpm_id');
		click = clickEvent();
		input.dispatchEvent(click);
	};

	$scope.save_cpm = function() {
		var modalInstance = $uibModal.open({
			templateUrl: 'save_cpm_box.html',
			controller: 'SaveController'
			});
	};

	$scope.open_cpm_select = function (selected) {
		var reader = new FileReader();
		var file_splitted = selected.files[0].name.split('.');
		var extension = file_splitted[file_splitted.length-1];

		reader.onload = function(event){
			// open a new clean prmt
			$scope.new_cpm();

			// if leading a cpm file, the file content is the text
			if(extension == 'cpm'){
				var text = event.target.result;

			// in a zipped file, the content must first be decompressed
			}else if(extension == 'zip'){
				var zip = new JSZip();
				zip.load(event.target.result);
				var text = zip.file(/.cpm/)[0].asText();
			};

			// store the original cpm filename in the headers of cpm
			if(extension == 'cpm'){
				vars.prmt['header'].cpm_s_filename = selected.files[0].name;

			}else if(extension == 'zip'){
				vars.prmt['header'].cpm_s_filename = zip.file(/.cpm/)[0].name;

				// also store the original zip filename in the headers of cpm
				vars.prmt['header'].zip_s_filename = selected.files[0].name;
			};

			// generate a json object from the cpm text
			vars.read_cpm(text);

			// run post-read code and force update all
			vars.post_read_cpm();

			// a zipped file may contain other files too
			if(extension == 'zip'){
				// add all graphdata slots
				graph.reset_simdata(vars.prmt.header.nSi);

				// open matrices
				vars.read_zipped_mtx(zip);

				// create the entries for simulation and experimental files
				vars.read_zipped_files(zip);

				// update exp graph
				graph.update_expdata();

			}
			// update all variables
			$rootScope.$apply();
			// update graph
			graph.update_graph();

			// delete file anchor to avoid problems reading same file
			var input = document.getElementById('open_cpm_id');
			input.value = null;
		};

		if(extension == 'cpm'){
			reader.readAsText(selected.files[0]);
		}
		else if(extension == 'zip'){
			reader.readAsArrayBuffer(selected.files[0]);
		};
	};
});

// create the controller and inject Angular's factories
App.controller('graphController', function($rootScope, $scope, vars, status, graph, connect) {
	$scope.status = status.status;
	$scope.progbar = connect.progbar;
	$scope.files = vars.files;
	$scope.graph = graph;

	var timer;
	var checker = function() {
		var pageload_completed = document.getElementById("myDiv").style.display == 'block';
		if(pageload_completed) {
			graph.create_graph();
			clearInterval(timer);
		}
	};
	timer = setInterval(checker, 10);

});

// create the controller and inject Angular's factories
App.controller('systemController', function($rootScope, $scope, vars, status, graph) {
	$scope.prmt = vars.prmt;
	$scope.files = vars.files;
	$scope.status = status.status;
	$scope.range = range;
	$scope.graph = graph;

	//----------------------------------------------------------------------

	$scope.open_exp = function() {
		var obj = document.getElementById('open_exp_id');
		click = clickEvent();
		obj.dispatchEvent(click);
	};

	$scope.open_exp_select = function (selected) {
		var reader = new FileReader();
		reader.onload = function(event){
			var iEx = 1;
			graph.reset_expdata();

			var x = Array();
			var y = Array();
			var data;
			var text = event.target.result;
			var lines = text.split('\n');
			// new graph values
			for(i=0; i<lines.length; i++){
				data = lines[i].trim().split(/[\t\s]+/);
				x.push(parseFloat(data[0]));
				y.push(parseFloat(data[1]));
			};
			graph.load_expdata(x, y, selected.files[0].name);

			// force update variables
			$rootScope.$apply();

			// update graph
			graph.update_expdata();

			// delete file anchor to avoid problems reading same file
			var input = document.getElementById('open_exp_id');
			input.value = null;
		};
		reader.readAsText(selected.files[0]);
	};

	// watch for any change in the graph object, then re-render it
	$scope.$watch('[prmt.output.norm, prmt.output.chi2red_s_xmin, prmt.output.chi2red_s_xmax]', function(newVal, oldVal){
		// update graph
		graph.update_graph();
		graph.calculate_chi2();
	});

	// watch for any change in the graph object, then re-render it
	$scope.$watch('[prmt.output.type]', function(newVal, oldVal){
		// update graph
		graph.change_layout();
		graph.update_graph();
	});

	// watch for any change in the graph object, then re-render it
	$scope.$watch('[prmt.expdata_s_1.a, prmt.expdata_s_1.b]', function(newVal, oldVal){
		// update graph
		graph.update_expdata();
	});

	$scope.calculate_norm = function() {
		graph.calculate_norm();
	};
	$scope.calculate_chi2 = function() {
		graph.calculate_chi2();
	};

});

// create the controller and inject Angular's factories
App.controller('sampleController', function($rootScope, $scope, $uibModal, vars, status) {
	$scope.prmt = vars.prmt;
	$scope.files = vars.files;
	$scope.status = status.status;
	$scope.range = range;
	$scope.range_inv = range_inv;

	$scope.switch_line = function(obj_type, i_str, change){
		i = parseInt(i_str);
		f = i + change;
		if(f < 1) return;

		var nEl = parseInt(vars.prmt.header.nEl);
		var nCp = parseInt(vars.prmt.header.nCp);
		var nMt = parseInt(vars.prmt.header.nMt);
		var nLa = parseInt(vars.prmt.header.nLa);

		if(obj_type == 'element'){
			if(f > nEl) return;

			var obj_i = vars.prmt['element_s_'+i];
			var obj_f = vars.prmt['element_s_'+f];

			for(iCp=1; iCp<=nCp; iCp++){
				var buffer = vars.prmt['compound_s_'+iCp]['_p_element_s_'+f];
				vars.prmt['compound_s_'+iCp]['_p_element_s_'+f] = vars.prmt['compound_s_'+iCp]['_p_element_s_'+i];
				vars.prmt['compound_s_'+iCp]['_p_element_s_'+i] = buffer;
				delete buffer;
			};
		};
		if(obj_type == 'compound'){
			if(f > nCp) return;

			var obj_i = vars.prmt['compound_s_'+i];
			var obj_f = vars.prmt['compound_s_'+f];
		};
		if(obj_type == 'matrix'){
			if(f > nMt) return;

			var obj_i = vars.prmt['matrix_s_'+i];
			var obj_f = vars.prmt['matrix_s_'+f];

			for(iLa=1; iLa<=nLa; iLa++){
				var buffer = vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+f];
				vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+f] = vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+i];
				vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+i] = buffer;
			};

		};
		if(obj_type == 'layer'){
			if(f > nLa) return;

			var obj_i = vars.prmt['layer_s_'+i];
			var obj_f = vars.prmt['layer_s_'+f];
		};

		var buffer = {};
		for (var attr in obj_f){
			buffer[attr] = obj_f[attr];
		};
		for (var attr in obj_i){
			obj_f[attr] = obj_i[attr];
		};
		for (var attr in buffer){
			obj_i[attr] = buffer[attr];
			delete buffer[attr];
		};

	};

	$scope.create_element = function(){
		vars.prmt.header.nEl = parseInt(vars.prmt.header.nEl)+1;
		var iEl = parseInt(vars.prmt.header.nEl);
		vars.prmt['element_s_'+iEl] = {};
		vars.prmt['element_s_'+iEl].name = 'Elem. '+iEl;
		vars.prmt['element_s_'+iEl].sig0 = '0';
		vars.prmt['element_s_'+iEl].Er_s_mean = '0';
		vars.prmt['element_s_'+iEl].Er_s_width = '0';

		// initiate all element fractions
		var nCp = parseInt(vars.prmt.header.nCp);
		for(iCp=1; iCp<=nCp; iCp++) vars.prmt['compound_s_'+iCp]['_p_element_s_'+iEl] = 0;

		var modalInstance = $uibModal.open({
			templateUrl: 'static/html/ptab.html',
			windowClass: 'app-modal-window',
			controller: 'PTabController',
			resolve: {
				element_index: function () {
					return iEl;
				}
			}
		});

		// open the advanced options for this element
		//$scope.status.samplePane.elem_selected = String(iEl);
	};

	$scope.create_compound = function(){
		vars.prmt.header.nCp = parseInt(vars.prmt.header.nCp)+1;
		var iCp = parseInt(vars.prmt.header.nCp);
		vars.prmt['compound_s_'+iCp] = {};
		vars.prmt['compound_s_'+iCp].name = 'Comp. '+iCp;

		// initiate all element fractions
		var nEl = parseInt(vars.prmt.header.nEl);
		for(iEl=1; iEl<=nEl; iEl++) vars.prmt['compound_s_'+iCp]['_p_element_s_'+iEl] = 0;

		// open the advanced options for this compound
		$scope.status.samplePane.comp_selected = String(iCp);
	};

	$scope.create_plasmon = function(iCp){
		if(typeof vars.prmt['compound_s_'+iCp].nPlasmons == 'undefined') vars.prmt['compound_s_'+iCp].nPlasmons = 0;
		vars.prmt['compound_s_'+iCp].nPlasmons = parseInt(vars.prmt['compound_s_'+iCp].nPlasmons)+1;
		var iPl = parseInt(vars.prmt['compound_s_'+iCp].nPlasmons);
		vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPl+'_s_A'] = 1;
		vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPl+'_s_w0'] = 0;
		vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPl+'_s_gamma'] = 1;
	};

	$scope.create_layer = function(){
		vars.prmt.header.nLa = parseInt(vars.prmt.header.nLa)+1;
		var iLa = parseInt(vars.prmt.header.nLa);
		vars.prmt['layer_s_'+iLa] = {};
		vars.prmt['layer_s_'+iLa].dLx = '0';
		vars.prmt['layer_s_'+iLa].dLy = '0';
		vars.prmt['layer_s_'+iLa].dLz = '0';
		vars.prmt['layer_s_'+iLa].periodic_s_contourn = 'yes';
		vars.prmt['layer_s_'+iLa].periodic_s_position = 'no';

		// initiate all matrix fractions
		var nMt = parseInt(vars.prmt.header.nMt);
		for(iMt=1; iMt<=nMt; iMt++) vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+iMt] = 0;
	};

	$scope.create_matrix = function(){
		vars.prmt.header.nMt = parseInt(vars.prmt.header.nMt)+1;
		var iMt = parseInt(vars.prmt.header.nMt);
		vars.prmt['matrix_s_'+iMt] = {};
		vars.prmt['matrix_s_'+iMt].file = 'matrix_s_'+iMt;
		vars.files['matrix_s_'+iMt] = {};
		vars.files['matrix_s_'+iMt].text = '';

		// initiate all matrix fractions
		var nLa = parseInt(vars.prmt.header.nLa);
		for(iLa=1; iLa<=nLa; iLa++) vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+iMt] = 0;
	};

	$scope.destroy_element = function(index){
		index = parseInt(index);
		var nEl = parseInt(vars.prmt.header.nEl);
		vars.prmt.header.nEl = parseInt(vars.prmt.header.nEl)-1;
		for(iEl=index; iEl<nEl; iEl++){
			var iElnext = iEl+1;
			vars.prmt['element_s_'+iEl] = vars.prmt['element_s_'+iElnext];
		};
		delete vars.prmt['element_s_'+nEl];

		var nCp = parseInt(vars.prmt.header.nCp);
		for(iCp=1; iCp<=nCp; iCp++){
			for(iEl=index; iEl<=nEl-1; iEl++){
				var iElnext = iEl+1;
				vars.prmt['compound_s_'+iCp]['_p_element_s_'+iEl] = vars.prmt['compound_s_'+iCp]['_p_element_s_'+iElnext];
			};
			delete vars.prmt['compound_s_'+iCp]['_p_element_s_'+nEl];
		};

	};
	$scope.destroy_compound = function(index){
		index = parseInt(index);
		var nCp = parseInt(vars.prmt.header.nCp);
		vars.prmt.header.nCp = parseInt(vars.prmt.header.nCp)-1;
		for(iCp=index; iCp<=nCp-1; iCp++){
			var iCpnext = iCp+1;
			vars.prmt['compound_s_'+iCp] = vars.prmt['compound_s_'+iCpnext];
		};
		delete vars.prmt['compound_s_'+nCp];
	};

	$scope.destroy_plasmon = function(iCp, index){
		index = parseInt(index);
		var nPl = parseInt(vars.prmt['compound_s_'+iCp].nPlasmons);
		vars.prmt['compound_s_'+iCp].nPlasmons = nPl-1;
		for(iPl=index; iPl<=nPl-1; iPl++){
			var iPlnext = iPl+1;
			vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPl+'_s_A'] = vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPlnext+'_s_A'];
			vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPl+'_s_w0'] = vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPlnext+'_s_w0'];
			vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPl+'_s_gamma'] = vars.prmt['compound_s_'+iCp]['plasmon_s_'+iPlnext+'_s_gamma'];
		};
		delete vars.prmt['compound_s_'+iCp]['plasmon_s_'+nPl+'_s_A'];
		delete vars.prmt['compound_s_'+iCp]['plasmon_s_'+nPl+'_s_w0'];
		delete vars.prmt['compound_s_'+iCp]['plasmon_s_'+nPl+'_s_gamma'];
	};

	$scope.destroy_layer = function(index){
		index = parseInt(index);
		var nLa = parseInt(vars.prmt.header.nLa);
		vars.prmt.header.nLa = parseInt(vars.prmt.header.nLa)-1;
		for(iLa=index; iLa<=nLa-1; iLa++){
			var iLanext = iLa+1;
			vars.prmt['layer_s_'+iLa] = vars.prmt['layer_s_'+iLanext];
		};
		delete vars.prmt['layer_s_'+nLa];
	};

	$scope.destroy_matrix = function(index){
		index = parseInt(index);
		var nMt = parseInt(vars.prmt.header.nMt);
		vars.prmt.header.nMt = parseInt(vars.prmt.header.nMt)-1;
		for(iMt=index; iMt<=nMt-1; iMt++){
			var iMtnext = iMt+1;
			vars.prmt['matrix_s_'+iMt] = vars.prmt['matrix_s_'+iMtnext];
		};
		delete vars.prmt['matrix_s_'+nMt];
		delete vars.files['matrix_s_'+nMt];

		var nLa = parseInt(vars.prmt.header.nLa);
		for(iLa=1; iLa<=nLa; iLa++){
			for(iMt=index; iMt<=nMt-1; iMt++){
				var iMtnext = iMt+1;
				vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+iMt] = vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+iMtnext];
			};
			delete vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+nMt];
		};
	};

	$scope.upload_matrix = function(index) {
		$scope.matrix_index = index;
		var obj = document.getElementById('upload_matrix_'+String(index));
		click = clickEvent();
		obj.dispatchEvent(click);
	};

	$scope.upload_matrix_select = function (selected) {
		var file = selected.files[0];
		var file_splited = file.name.split('.');
		var extension = file_splited[file_splited.length-1];

		var reader = new FileReader();
		reader.onload = function(event){
			var iMt = $scope.matrix_index;
			vars.prmt['matrix_s_'+iMt].file = file.name;
			vars.files['matrix_s_'+iMt].text = event.target.result;

			if(extension == 'mtx'){
				vars.prmt['matrix_s_'+iMt].type = 'pM3';

				var lines = vars.files['matrix_s_'+iMt].text.split('\n');
				var numbers = lines[0].trim().split(/\s/);
				if(numbers.length >= 3) {
					vars.prmt['matrix_s_'+iMt].nx = numbers[0];
					vars.prmt['matrix_s_'+iMt].ny = numbers[1];
					vars.prmt['matrix_s_'+iMt].nz = numbers[2];
				}
				else{
					vars.prmt['matrix_s_'+iMt].nx = 0;
					vars.prmt['matrix_s_'+iMt].ny = 0;
					vars.prmt['matrix_s_'+iMt].nz = 0;
				};
				if(numbers.length >= 4)
					vars.prmt['matrix_s_'+iMt].phi = numbers[3]
				else
					vars.prmt['matrix_s_'+iMt].phi = 0;
			};
			if(extension == 'prf' || extension == 'prf2')
				vars.prmt['matrix_s_'+iMt].type = 'TRI3DYN';
			if(extension == 'vox')
				vars.prmt['matrix_s_'+iMt].type = 'vox';

			$rootScope.$apply();
		};
		if(extension == 'vox')
			reader.readAsArrayBuffer(file);
		else
			reader.readAsText(file);
	};

	// open viasualization page
	$scope.visualize_matrix = function(index, text) {
		var modalInstance = $uibModal.open({
			templateUrl: 'static/html/visualizer.html',
			windowClass: 'app-modal-window',
			controller: 'VisualizerController',
			resolve: {
				matrix_index: function () {
					return index;
				},
				matrix_text: function () {
					return text;
				}
			}
		});
	};

	// test if there is a matrix
	$scope.isThereMatrixInThisLayer = function(iLa){
		var nMt = parseInt(vars.prmt.header.nMt);
		var nLa = parseInt(vars.prmt.header.nLa);
		for(iMt=1; iMt<=nMt; iMt++){
			if(vars.prmt['layer_s_'+iLa]['_p_matrix_s_'+iMt] != '0') return true;
		};
		return false;
	};
});

App.controller('SaveController', function($uibModalInstance, $scope, vars, status) {
	$scope.save_simulation_filename = 'output.dat';
	$scope.save_zip_filename = vars.prmt.header.zip_s_filename;

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.save_cpm_save = function () {
		zipData = vars.generate_zip($scope.save_zip_filename);

		var obj = document.getElementById('anchor_for_cpm_save');
		var file = new Blob([zipData], {type: 'application/zip'});
		obj.href = URL.createObjectURL(file);
		obj.download = $scope.save_zip_filename;

		click = clickEvent();
		obj.dispatchEvent(click);

		$uibModalInstance.dismiss('cancel');
	};
});

App.controller('PTabController', function($uibModalInstance, $scope, element_index, vars) {
	$(function(){
		$('li[class^="type-"]').mouseover(function(){
			var currentClass = $(this).attr('class').split(' ')[0];
			if(currentClass != 'empty'){
				$('.main > li').addClass('deactivate');
				$('.' + currentClass).removeClass('deactivate');
			}
		});

		$('li[class^="cat-"]').mouseover(function(){
			var currentClass = $(this).attr('class').split(' ')[0];
			$('.main > li').addClass('deactivate');
			$('.' + currentClass).removeClass('deactivate');
		});

		$('.main > li').mouseout(function(){
			var currentClass = $(this).attr('class').split(' ')[0];
			$('.main > li').removeClass('deactivate');
		});
	});

	// struct with all the periodic elements data
	$scope.ptab = ptab;

	$scope.select_element = function(element){
		vars.prmt['element_s_'+element_index].name = element.symb;
		//vars.prmt['element_s_'+element_index].M = element.M;
		vars.prmt['element_s_'+element_index].M = 'All Isotopes';
		vars.prmt['element_s_'+element_index].Z = element.Z;
		$uibModalInstance.dismiss('cancel');
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

function guid(){
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
					function(c) {
						var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
						return v.toString(16);
					});
};


function clickEvent(){
	var click = new MouseEvent('click', {
			'view': window,
			'bubbles': true,
			'cancelable': false
		});
	return click;
};

App.controller('VisualizerController', function($uibModalInstance, $scope, matrix_index, matrix_text, vars, status) {
        $scope.prmt = vars.prmt;
    $scope.files = vars.files;
    $scope.status = status.status;
    $scope.range = range;

    if(vars.prmt['matrix_s_'+matrix_index].type == "vox"){
		var buffer = vars.files['matrix_s_'+matrix_index].text;

		var full_file = new DataView(buffer);
		var binary_size = full_file.byteLength;

		// first we find the start position of the voxels in the binary file
		var bin = Array(4);
		for(n=0; n<binary_size-4; n+=4){
			bin[0] = new DataView(buffer, n).getInt8();
			bin[1] = new DataView(buffer, n+1).getInt8();
			bin[2] = new DataView(buffer, n+2).getInt8();
			bin[3] = new DataView(buffer, n+3).getInt8();
			//console.log(bin[0], bin[1], bin[2], bin[3])
			//console.log(String.fromCharCode(bin[0]), String.fromCharCode(bin[1]), String.fromCharCode(bin[2]), String.fromCharCode(bin[3]));

			// in ASCII: S = 83, I = 73, Z = 90, E = 69
			if(bin[0] == 83 && bin[1] == 73 && bin[2] == 90 && bin[3] == 69){
				var nx = new DataView(buffer, n+12).getInt8();
				var ny = new DataView(buffer, n+16).getInt8();
				var nz = new DataView(buffer, n+20).getInt8();
			}

			// in ASCII: X = 88, Y = 89, Z = 90, I = 73
			if(bin[0] == 88 && bin[1] == 89 && bin[2] == 90 && bin[3] == 73){
				var initial_index = n;
				break;
			}
		}
		// instantiate the matrix
		var matrix = [];
		for(i=0; i<nx; i++){
			matrix.push([]);
			for(j=0; j<ny; j++){
				matrix[i].push([]);
				for(k=0; k<nz; k++)
					matrix[i][j].push(0);
			}
		}

		// must jump 16 bytes to get over ??? segment
		initial_index += 16;

		var i; var j; var k; var Cp;
		for(n=initial_index; n<binary_size-4; n+=4){
			i = new DataView(buffer, n).getInt8();
			j = new DataView(buffer, n+1).getInt8();
			k = new DataView(buffer, n+2).getInt8();
			Cp = new DataView(buffer, n+3).getInt8();
			console.log(n+' '+i+' '+j+' '+k+' '+Cp);
			matrix[i][j][k] = Cp;
			if(i == nx-1 && j == ny-1  && k == nz-1) break;
		}

		return;
	}

    var matrix_lines = matrix_text.split('\n');
    var random = Math.random;

    var line = matrix_lines[0].split(' ');
    var nx = parseInt(line[0]);
    var ny = parseInt(line[1]);
    var nz = parseInt(line[2]);
    //var k = parseInt(matrix_lines[1].split(' ')[0]);
    //var e = parseInt(matrix_lines[1].split(' ')[1]);
    //var n2 = parseInt(matrix_lines[1].split(' ')[0]);

    var p = 1e4/(nx*ny*nz);

    // Create and populate a data table.
    var data = new vis.DataSet();

    var counter = 0;

    var xaxisMax = nx;
    var yaxisMax = ny;
    var zaxisMax = nz;
    $scope.A = Array();
    $scope.V = Array();
    $scope.M = Array();
    $scope.CB = Array();
    var i=1;
    var n2 = 0;
    var dL = parseFloat(vars.prmt.simulation.dL);



    //  while($scope.A.length <= e) $scope.A.push(0);
    //$scope.A[e] = $scope.A[e] + k;
    for (var z = 1; z <= nz; z++)
        for (var x = 1; x <= nx; x+=1)
                for (var y = 1; y <= ny; y+=1) {
                    n = (z-1)*nx*ny+ny*(x-1)+y;
                   if(n>n2) {
                    line = matrix_lines[i].split(' ');
                     k = parseInt(line[0]);
                     n2 = n2 + k;
                     e = parseInt(line[1]);
                         while($scope.A.length <= e) $scope.A.push(0);
                      	 while($scope.V.length <= e) $scope.V.push(0);
                    	 while($scope.M.length <= e) $scope.M.push(0);
                         $scope.A[e] = $scope.A[e] + parseFloat(k);
                         $scope.V[e] = $scope.V[e] + 100*Math.pow(10,-22)*parseFloat(k)*Math.pow(dL,3);

                    	if (e > 0) $scope.M[e] = $scope.M[e] + parseFloat(k)*parseFloat(vars.prmt["compound_s_"+e].density)*Math.pow(dL*1E-24, 3);
                     i = i + 1;
                     }
               //$scope.refresh = function() { for (i=1;i< $scope.A.lenght; i++) {
                	if(random() < p && e != 0)  //&& CB[i] == false)
                    	data.add({id:counter++,x:x,y:y,z:nz-z,style:e});
            	}
                //}
                //};
                $scope.M[0]=0;
  /*  for(i=0;i<$scope.A.lenght;i++) {
    	$scope.M[i] = Math.round($scope.M[i].toFixed(2)*100)/100;
    	$scope.V[i] = Math.round($scope.V[i].toFixed(2)*100)/100;
    }
    */

    $scope.strV = Array();
    $scope.strM = Array();

    for(i=1;i<$scope.V.length;i++) {
    	$scope.strV.push(String($scope.V[i]));
    	var n1 = RegExp(".\...");
    	var e1 = RegExp("e+?-?[0-9][0-9]");
    	var n = n1.exec($scope.V[i]);
    	var e = e1.exec($scope.V[i]);
    	$scope.strV[i] = n + e;
    }

    for(i=1;i<$scope.M.length;i++) {
    	$scope.strM.push(String($scope.M[i]));
    	var n1 = RegExp(".\...");
    	var e1 = RegExp("e+?-?[0-9][0-9]");
    	var n = n1.exec($scope.M[i]);
    	var e = e1.exec($scope.M[i]);
    	$scope.strM[i] = n + e;
    }

    $scope.sizeicp = $scope.A.lenght;

    // specify options
    var options = {
        width:  '600px',
        height: '600px',
        style: 'dot-color',
        showPerspective: true,
        showGrid: true,
        showShadow: false,
        keepAspectRatio: true,
        verticalRatio: 1.0*nz/nx
    };

    $scope.teste = function(){
        setTimeout(function(){
            var container = document.getElementById('visualization');
            graph3d = new vis.Graph3d(container, data, options);
        }, 100);

	};
});

function range(n){
	var A = Array();
	for(i=1; i<=n; i++) A.push(String(i))
	return A;
};

function range_inv(n){
	var A = Array();
	for(i=n; i>=1; i--) A.push(String(i))
	return A;
};

function interpolate(data, x){
	var high_side = null;
	var low_side = null;

	if(data.x[0] < data.x[1]){
		for(i=0; i<data.length; i++)
			if(data.x[i] > x){
				high_side = i;
				low_side = i-1;
				break;
			};
	};
	if(data.x[0] > data.x[1]){
		for(i=data.length-1; i>-1; i--)
			if(data.x[i] > x){
				high_side = i;
				low_side = i-1;
				break;
			};
	};

	if(	high_side == null ||
		high_side < 0 ||
		high_side >= data.length ||
		low_side == null ||
		low_side < 0 ||
		low_side >= data.length ) return null;

	dx = data.x[high_side] - data.x[low_side];
	slope = (data.y[high_side] - data.y[low_side]) / dx;
	inter = data.y[low_side] + (x - data.x[low_side]) * slope;

	return inter;
};
