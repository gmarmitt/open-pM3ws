// create the module and name it app
var App = angular.module('App', ['ui.bootstrap']);

// create the controller and inject Angular's $rootScope
App.controller('AppController', function($scope, $http, $filter, $uibModal) {
	// all information inputted is stored in 'info'
	$scope.info = {}
	// support struct for other variables
	$scope.sup = {}
	
	$scope.s = sender_details;

	//number of samples
	$scope.info.nSa = 5;

	// initiate the input variables
	initialize_prmt($scope);

	// sends the info to server
	$scope.send_info = function(){
		// validate the form
		var err = $scope.validation();
		if(err != 0){
			create_message_window(err);
			return;
		};
		
		// generate a unique id to communicate with the server
		$scope.sup.uid = guid();
		
		payload = [$scope.sup.uid, $scope.info, $scope.get_sender($scope.serial)]
		$http.post('/start_rrt', $filter('json')(payload));
		create_message_window('');
	};

	$scope.upload_data = function(index) {
		$scope.sup.data_index = index;
		var obj = document.getElementById('upload_data_'+String(index));
		click = clickEvent();
		obj.dispatchEvent(click);
	};

	$scope.upload_data_select = function (selected) {
		var file = selected.files[0];
		var reader = new FileReader();
		reader.onload = function(event){
			$scope.info.data[$scope.sup.data_index].text = event.target.result;
			$scope.info.data[$scope.sup.data_index].file = file.name;
			$scope.info.data[$scope.sup.data_index].size = file.size;
			$scope.$apply();
		};
		reader.readAsText(file);
	};

	$scope.validation = function () {
		// all forms validation
		var forms = [document.getElementsByName('beamform'), document.getElementsByName('deteform'), document.getElementsByName('dataform')];
		for(f=0; f<forms.length; f++){
			// each form is actually an array with an element for each sample
			var forms_samples = forms[f];
			for(s=0; s<forms_samples.length; s++){
				// a particular form element array
				var elements = forms_samples[s].elements;
				for(e=0; e<elements.length; e++){
					var err = '';
					var attrs = elements[e].attributes;
					var attrs_name = attrs.getNamedItem('name').value;
					var attrs_class = attrs.getNamedItem('class').value.split(' ');
					// check for any error flag inside the element class
					for(var a=0; a<attrs_class.length; a++){
						if(attrs_class[a] == 'ng-invalid-required') err = '"'+attrs_name+'" must be filled.';
						if(attrs_class[a] == 'ng-invalid-pattern') err = '"'+attrs_name+'" must be a number.';
					};
					// if any error is found, print and then exit the function
					if(err != ''){
						err = 'In "'+$scope.info.prmt[s].header.name+'", the value of '+err;
						return err;
					};
				};
			};
		};
		return 0;
	};


	create_message_window = function (err){
		var modalInstance = $uibModal.open({
			templateUrl: 'static/html/message.html',
			controller: 'MessageController',
			windowClass: 'app-modal-window',
			resolve: {
				message: function () {
					return err;
					}
			}
		});
	};

	$scope.get_sender = function (serial){
		if(serial == 1764) return sender_details[0];
		if(serial == 8465) return sender_details[1];
		if(serial == 5372) return sender_details[2];
		if(serial == 1092) return sender_details[3];
		if(serial == 8752) return sender_details[4];
		if(serial == 5154) return sender_details[5];
		if(serial == 4873) return sender_details[6];
		if(serial == 7207) return sender_details[7];
		if(serial == 9543) return sender_details[8];
		if(serial == 1361) return sender_details[9];
		if(serial == 3054) return sender_details[10];
		if(serial == 6921) return sender_details[11];
		return null;
	};
});

App.controller('MessageController', function($uibModalInstance, $scope, message) {
	$scope.init = function () {
		var box = document.getElementById('message_box');
		if(message == ''){
			box.innerHTML = '<h2 style="color:black;">Data sent</h2><p>Thank you for participating!</p>';
		}
		else{
			box.innerHTML = '<h2 style="color:black;">Error:</h2><p>'+message+'</p>';
		}
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

function guid(){
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
					function(c) {
						var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
						return v.toString(16);
					});
};

function clickEvent(){
	var click = new MouseEvent('click', {
			'view': window,
			'bubbles': true,
			'cancelable': false
		});
	return click;
};

function initialize_prmt($scope){
	$scope.info.prmt = []
	$scope.info.data = []
	
	for(iSa=0; iSa<$scope.info.nSa; iSa++){
		$scope.info.data.push({})
		$scope.info.data[iSa].text = ''
		$scope.info.data[iSa].file = ''
		$scope.info.data[iSa].size = ''

		$scope.info.prmt.push({})

		$scope.info.prmt[iSa].header = {};
		$scope.info.prmt[iSa].header.version = '3.00';
		$scope.info.prmt[iSa].header.obs = '...';
		$scope.info.prmt[iSa].header.index = String(iSa);

		$scope.info.prmt[iSa].simulation = {};
		$scope.info.prmt[iSa].simulation.cores = '1';
		$scope.info.prmt[iSa].simulation.nI = '5e7';
		$scope.info.prmt[iSa].simulation.dL = '1';
		$scope.info.prmt[iSa].simulation.dr = '1';
		$scope.info.prmt[iSa].simulation.line_s_shape = 'EMG';
		$scope.info.prmt[iSa].simulation.cross_s_section = 'correc';
		$scope.info.prmt[iSa].simulation.algorithm = 'ssct';

		$scope.info.prmt[iSa].calculations = {};
		$scope.info.prmt[iSa].calculations.stopping = 'stop96';
		$scope.info.prmt[iSa].calculations.straggling = 'Chu';

		$scope.info.prmt[iSa].output = {};
		$scope.info.prmt[iSa].output.Emin = '30';
		$scope.info.prmt[iSa].output.Emax = '200';
		$scope.info.prmt[iSa].output.dEnergy = '0.2';
		$scope.info.prmt[iSa].output.dAngle = '0.5';
		$scope.info.prmt[iSa].output.histogram = 'simple';

		$scope.info.prmt[iSa].beam = {};
		$scope.info.prmt[iSa].beam.type = 'ion';
		$scope.info.prmt[iSa].beam.Z = '';
		$scope.info.prmt[iSa].beam.M = '';
		$scope.info.prmt[iSa].beam.Q = '1';
		$scope.info.prmt[iSa].beam.E0 = '';
		$scope.info.prmt[iSa].beam.theta = '';

		$scope.info.prmt[iSa].detector = {};
		$scope.info.prmt[iSa].detector.type = 'electro';
		$scope.info.prmt[iSa].detector.resolution = '';
		$scope.info.prmt[iSa].detector.Y0 = '';
		$scope.info.prmt[iSa].detector.LY = '';
		$scope.info.prmt[iSa].detector.X0 = '0';
		$scope.info.prmt[iSa].detector.LX = '0';
		$scope.info.prmt[iSa].detector.TOF_s_distance = '1';
		$scope.info.prmt[iSa].detector.TOF_s_resolution = '10';

		$scope.info.prmt[iSa].element_s_1 = {};
		$scope.info.prmt[iSa].element_s_1.name = 'Si';
		$scope.info.prmt[iSa].element_s_1.Z = '14';
		$scope.info.prmt[iSa].element_s_1.M = '28';
		$scope.info.prmt[iSa].element_s_1.sig0 = '0';

		$scope.info.prmt[iSa].element_s_2 = {};
		$scope.info.prmt[iSa].element_s_2.name = 'O';
		$scope.info.prmt[iSa].element_s_2.Z = '8';
		$scope.info.prmt[iSa].element_s_2.M = '16';
		$scope.info.prmt[iSa].element_s_2.sig0 = '0';

		$scope.info.prmt[iSa].element_s_3 = {};
		$scope.info.prmt[iSa].element_s_3.name = 'Hf';
		$scope.info.prmt[iSa].element_s_3.Z = '72';
		$scope.info.prmt[iSa].element_s_3.M = '178.5';
		$scope.info.prmt[iSa].element_s_3.sig0 = '0';

		$scope.info.prmt[iSa].compound_s_1 = {};
		$scope.info.prmt[iSa].compound_s_1.name = 'Si';
		$scope.info.prmt[iSa].compound_s_1.density = '2.33';
		$scope.info.prmt[iSa].compound_s_1._p_element_s_1 = '1';
		$scope.info.prmt[iSa].compound_s_1._p_element_s_2 = '0';
		$scope.info.prmt[iSa].compound_s_1._p_element_s_3 = '0';

		$scope.info.prmt[iSa].compound_s_2 = {};
		$scope.info.prmt[iSa].compound_s_2.name = 'SiO2';
		$scope.info.prmt[iSa].compound_s_2.density = '2.65';
		$scope.info.prmt[iSa].compound_s_2._p_element_s_1 = '1';
		$scope.info.prmt[iSa].compound_s_2._p_element_s_2 = '2';
		$scope.info.prmt[iSa].compound_s_2._p_element_s_3 = '0';

		$scope.info.prmt[iSa].compound_s_3 = {};
		$scope.info.prmt[iSa].compound_s_3.name = 'HfO2';
		$scope.info.prmt[iSa].compound_s_3.density = '9.68';
		$scope.info.prmt[iSa].compound_s_3._p_element_s_1 = '0';
		$scope.info.prmt[iSa].compound_s_3._p_element_s_2 = '2';
		$scope.info.prmt[iSa].compound_s_3._p_element_s_3 = '1';
	};

	$scope.info.prmt[0].header.name = 'Sample 1 nm';
	$scope.info.prmt[0].layer_s_1 = {};
	$scope.info.prmt[0].layer_s_1.thickness = '1000';
	$scope.info.prmt[0].layer_s_1.compound = '1';
	$scope.info.prmt[0].layer_s_2 = {};
	$scope.info.prmt[0].layer_s_2.thickness = '20';
	$scope.info.prmt[0].layer_s_2.compound = '2';
	$scope.info.prmt[0].layer_s_3 = {};
	$scope.info.prmt[0].layer_s_3.thickness = '10';
	$scope.info.prmt[0].layer_s_3.compound = '3';

	$scope.info.prmt[1].header.name = 'Sample 3 nm';
	$scope.info.prmt[1].layer_s_1 = {};
	$scope.info.prmt[1].layer_s_1.thickness = '1000';
	$scope.info.prmt[1].layer_s_1.compound = '1';
	$scope.info.prmt[1].layer_s_2 = {};
	$scope.info.prmt[1].layer_s_2.thickness = '20';
	$scope.info.prmt[1].layer_s_2.compound = '2';
	$scope.info.prmt[1].layer_s_3 = {};
	$scope.info.prmt[1].layer_s_3.thickness = '30';
	$scope.info.prmt[1].layer_s_3.compound = '3';

	$scope.info.prmt[2].header.name = 'Sample 5 nm';
	$scope.info.prmt[2].layer_s_1 = {};
	$scope.info.prmt[2].layer_s_1.thickness = '1000';
	$scope.info.prmt[2].layer_s_1.compound = '1';
	$scope.info.prmt[2].layer_s_2 = {};
	$scope.info.prmt[2].layer_s_2.thickness = '20';
	$scope.info.prmt[2].layer_s_2.compound = '2';
	$scope.info.prmt[2].layer_s_3 = {};
	$scope.info.prmt[2].layer_s_3.thickness = '50';
	$scope.info.prmt[2].layer_s_3.compound = '3';

	$scope.info.prmt[3].header.name = 'Sample 7 nm';
	$scope.info.prmt[3].layer_s_1 = {};
	$scope.info.prmt[3].layer_s_1.thickness = '1000';
	$scope.info.prmt[3].layer_s_1.compound = '1';
	$scope.info.prmt[3].layer_s_2 = {};
	$scope.info.prmt[3].layer_s_2.thickness = '20';
	$scope.info.prmt[3].layer_s_2.compound = '2';
	$scope.info.prmt[3].layer_s_3 = {};
	$scope.info.prmt[3].layer_s_3.thickness = '70';
	$scope.info.prmt[3].layer_s_3.compound = '3';

	$scope.info.prmt[4].header.name = 'Sample multiple-layered';
	$scope.info.prmt[4].layer_s_1 = {};
	$scope.info.prmt[4].layer_s_1.thickness = '1000';
	$scope.info.prmt[4].layer_s_1.compound = '1';
	$scope.info.prmt[4].layer_s_2 = {};
	$scope.info.prmt[4].layer_s_2.thickness = '20';
	$scope.info.prmt[4].layer_s_2.compound = '2';
	$scope.info.prmt[4].layer_s_3 = {};
	$scope.info.prmt[4].layer_s_3.thickness = '100';
	$scope.info.prmt[4].layer_s_3.compound = '3';
};

var sender_details = [
	{
		resp: 'Lyudmila Goncharova',
		addr: 'Western University, Canada'
	},
	{
		resp: 'Kimura Kenji',
		addr: 'Kyoto University, Japan'
	},
	{
		resp: 'Torgny Gustafsson',
		addr: 'Rutgers University, NJ-USA'
	},
	{
		resp: 'Matt Copel',
		addr: 'IBM T.J. Watson Research Center, NY-USA'
	},
	{
		resp: 'Rainer Loesing',
		addr: 'Globalfoundries, NY-USA'
	},
	{
		resp: 'Pedro Luis Grande',
		addr: 'Institute of Physics-UFRGS, Brazil'
	},
	{
		resp: '이형익',
		addr: 'Samsung, Korea'
	},
	{
		resp: '이성호',
		addr: 'Samsung, Korea'
	},
	{
		resp: '채근화',
		addr: 'KIST, Korea'
	},
	{
		resp: '민원자',
		addr: 'KMAC, Korea'
	},
	{
		resp: '문대원',
		addr: 'DGIST, Korea'
	},
	{
		resp: '고중규',
		addr: 'SK, Korea'
	}
]
