# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from multiprocessing.pool import ThreadPool
import configparser
import ctypes
import zipfile
import time
import math
import os
import sys

projectdir = 'open-pM3ws'

#add local libraries
home_path = os.path.dirname(os.path.realpath(__file__)).split('/'+projectdir)[0] + '/'
sys.path.append( home_path + 'open-pM3/' )
from plib.structs import *
import pM3

class Worker:
	def start(self, job):
		#opening variables
		self.err = 0					# job errors
		self.status = 1					# job status

		self.uid = job.uid					# job id
		self.nThreads = job.nThreads		# number of simulation threads
		self.zipfile = job.zipfile			# zipfile name

		# define paths
		self.paths = pM3.PATHS()

		# define input ctypes structs, these will be updated in load_config()
		self.prmt_c = None
		self.matrix_c = None
		self.tables_c = [None]

		# allocate memory for ctypes output structs
		progcount_c = (ctypes.c_long * self.nThreads)()
		histo_c = (SThisto * self.nThreads)()

		# create a new threads inside this process
		pool = ThreadPool(processes=2)

		# start the data collector
		data_thread = pool.apply_async(self.data_collector, (progcount_c, histo_c, job.queue_in, job.queue_out))

		# read the config file
		self.load_config()

		# start simulation
		if self.err == 0 and self.status > 0:
			pool.apply_async(self.start_simulation, (progcount_c, histo_c))

		# wait for the data collector to end
		data_thread.wait()

		# suice the whole process
		sys.exit()

	def data_collector(self, progcount_c, histo_c, queue_in, queue_out):
		# start sending data back to the parent process
		while True:
			# if there is no data in the queue
			if queue_out.empty() == True:
				# queue the result to the parent process
				queue_out.put(self.return_data(progcount_c, histo_c))

			# update the job status from the parent process
			if queue_in.empty() == False:
				self.status = queue_in.get()

			# if the stop command has been given
			if self.status == -1 or self.err != 0:
				# send the result back to the parent process
				queue_out.put(self.return_data(progcount_c, histo_c))
				# and ends the data collector
				return

			#print(self.status)
			# then wait a bit
			time.sleep(0.01)

	def load_config(self):
		# read the request cpm
		try:
			#create the configparser structure
			config = configparser.ConfigParser()
			config.optionxform=str

			# unzip the file and copy the config object
			with zipfile.ZipFile(self.zipfile) as myzip:
				myfile = myzip.read('pm3.cpm')
				cpm = myfile.decode('ascii')
				config.read_string(cpm)
		except:
			self.err = 300
			return None

		# open the configfile
		self.status = 20
		#print('Opening CPM...')
		try:
			pM3.term_input.prmt_FileDir = home_path+projectdir+'/jobs/'
			pM3.term_input.outp_FileName = home_path+projectdir+'/output.dat'
			[self.prmt_c, self.matrix_c, config] = pM3.OPEN_PARAMETERS_FILE(pM3.term_input, self.paths.isotopes, config)
		except:
			self.err = 301
			return None

		# open the matrix files
		self.status = 30
		#print('Generating matrix...')
		try:
			# unzip and copy the matrix data
			with zipfile.ZipFile(self.zipfile) as myzip:
				for iMt in range(1, self.prmt_c.nMt+1):
					MatrixName = config['matrix '+str(iMt)].get('file')
					try:
						with myzip.open(MatrixName) as myfile:
							data = myfile.read().decode('ascii')
							self.matrix_c[iMt].FileData = ctypes.c_char_p(data.encode())
					except:
						pass

			#run matgen functions
			pM3.MATGEN(self.prmt_c, self.matrix_c)

			if self.prmt_c.simu.algorithm == 1:
				config = pM3.REFACTOR_PARAMETERS(self.prmt_c, self.matrix_c, config)
				[self.prmt_c, self.matrix_c, config] = pM3.OPEN_PARAMETERS_FILE(pM3.term_input, self.paths.isotopes, config)
				pM3.MATGEN(self.prmt_c, self.matrix_c)

			# allocate memory and decompress the matrix data
			pM3.GENERATE_MATRIX_C(self.paths, self.prmt_c, self.matrix_c)
		except ValueError as err:
			self.err = int(str(err))
			return None

		# overwrite some attributes for better control
		self.prmt_c.simu.nThreads = self.nThreads

		# manage parameters changes
		try:
			pM3.PREPARATION(self.prmt_c, self.matrix_c)
		except:
			self.err = 302
			return None

		# open the calc tables
		try:
			self.tables_c[0] = pM3.OPEN_TABLES(self.prmt_c)
		except:
			self.err = 303
			return None

		# kinematic factor and cross sections
		if self.status > 0:
			self.status = 40
		else:
			return None

		#print('Kinematic factors...')
		try:
			pM3.K_CALCULATION(self.prmt_c, self.tables_c[0])
		except:
			self.err = 304
			return None
		#print('Cross sections...')
		pM3.CS_CALCULATION_CM(self.paths, self.prmt_c, self.tables_c[0])
		try:
			pass
		except:
			self.err = 305
			return None

		# stopping power calculation
		if self.status > 0:
			self.status = 50
		else:
			return None
		#print('Stopping power...')
		try:
			pM3.STOPPING_POWER_CALCULATION(self.paths, self.prmt_c, self.tables_c[0])
		except:
			self.err = 306
			return None

		# straggling calculation
		if self.status > 0:
			self.status = 60
		else:
			return None
		#print('Straggling...')
		try:
			pM3.STRAGG_CALCULATION(self.prmt_c, self.tables_c[0])
		except:
			self.err = 307
			return None

		if self.status > 0:
			self.status = 70
		else:
			return None

		#print('Charge fraction...')
		try:
			pM3.CHARGE_FRACTION_CALCULATION(self.paths, self.prmt_c, self.tables_c[0])
		except:
			self.err = 308
			return None

		if self.status > 0:
			self.status = 80
		else:
			return None
		#print('Dielectric function...')
		try:
			pM3.DIELECTRIC_FUNCTION(self.paths, self.prmt_c, self.tables_c[0])
		except:
			self.err = 309
			return None

		return None

	def start_simulation(self, progcount_c, histo_c):
		# run the simulation
		#print('\nRunning simulation:')
		try:
			self.status = 100
			if 	self.prmt_c.simu.algorithm == 1:
				self.histo_c = pM3.SIMU_TRAJ(self.paths, self.prmt_c, self.matrix_c, self.tables_c[0], histo_c, progcount_c, False, False)
			else:
				self.histo_c = pM3.SIMULA(self.paths, self.prmt_c, self.matrix_c, self.tables_c[0], histo_c, progcount_c, False, False)
		except ValueError as err:
			self.err = int(str(err))

		# gives the suicide signal
		self.status = -100
		return

	def return_data(self, progcount_c, histo_c):
		progcounts = 0
		for iThread in range(self.nThreads):
			progcounts += progcount_c[iThread]

		if self.status != 100:
			# if there is no simulation progress, send no data
			data = '"data": []'
			return [self.status, self.err, progcounts, data]

		else:
			# if there is simulation progress, start sending data
			data = '"data": ['

			# first, build the x axis
			data += '{"name":"x","energy":['
			for iEn in range(self.prmt_c.outp.nEn):
				energy = (iEn*self.prmt_c.outp.dEnergy+self.prmt_c.outp.Emin)/1e3
				data += str(energy)+','
			data = data[:-1] + '],"angle":[ '
			AngleMin = self.prmt_c.dete.Y0 - self.prmt_c.dete.LY/2
			for iAn in range(1, self.prmt_c.outp.nAn):
				data += str(math.degrees(iAn*self.prmt_c.outp.dAngle+AngleMin))+','
			data = data[:-1] + ']},'

			try:
				# obtain the total 2D histogram
				histo = pM3.OUTPUT(pM3.term_input, self.prmt_c, histo_c, False)
				temp = ''

				if self.prmt_c.outp.histo_type == 0:
					temp += '{"name":"Simulation","h1D":['
					for iEn in range(self.prmt_c.outp.nEn):
						counts = 0
						for iAn in range(self.prmt_c.outp.nAn):
							# normalize to the final expected nI
							counts += histo[iEn][iAn] * self.prmt_c.simu.nI/progcounts
						temp += str(counts)+','
					temp = temp[:-1] + '],"h2D":['
					for iEn in range(self.prmt_c.outp.nEn):
						temp += '[ '
						for iAn in range(1, self.prmt_c.outp.nAn):
							temp += '{0:.2g},'.format(histo[iEn][iAn])
						temp = temp[:-1] + '],'

					temp = temp[:-1] + ']},'

				elif self.prmt_c.outp.histo_type == 1:
					for iEl in range(1, self.prmt_c.nEl+1):
						temp += '{"name":"'+self.prmt_c.elem[iEl].name.decode()+'","h1D":['
						for iEn in range(self.prmt_c.outp.nEn):
							# normalize to the final expected nI
							counts = histo[iEn][iEl] * self.prmt_c.simu.nI/progcounts
							temp += str(counts)+','
						temp = temp[:-1] + '],"h2D":[[]]},'

				elif self.prmt_c.outp.histo_type == 2:
					for iCp in range(1, self.prmt_c.nCp+1):
						temp += '{"name":"'+self.prmt_c.comp[iCp].name.decode()+'","h1D":['
						for iEn in range(self.prmt_c.outp.nEn):
							# normalize to the final expected nI
							counts = histo[iEn][iCp] * self.prmt_c.simu.nI/progcounts
							temp += str(counts)+','
						temp = temp[:-1] + '],"h2D":[[]]},'

			except:
				temp = ''
				# if some problem occurs, send 0 counts
				if self.prmt_c.outp.histo_type == 0:
					temp += '{"name":"Simulation","h1D":['
					for iEn in range(self.prmt_c.outp.nEn):
						temp += str(0)+','
					temp = temp[:-1] + '],"h2D":[[]]},'

				if self.prmt_c.outp.histo_type == 1:
					for iEl in range(1, self.prmt_c.nEl+1):
						temp += '{"name":"'+self.prmt_c.elem[iEl].name.decode()+'","h1D":['
						for iEn in range(self.prmt_c.outp.nEn):
							temp += str(0)+','
						temp = temp[:-1] + '],"h2D":[[]]},'

				if self.prmt_c.outp.histo_type == 2:
					for iCp in range(1, self.prmt_c.nCp+1):
						temp += '{"name":"'+self.prmt_c.comp[iCp].name.decode()+'","h1D":['
						for iEn in range(self.prmt_c.outp.nEn):
							temp += str(0)+','
						temp = temp[:-1] + '],"h2D":[[]]},'

			# remove the last comma and close the vector
			data += temp[:-1] + ']'

			return [self.status, self.err, progcounts, data]
